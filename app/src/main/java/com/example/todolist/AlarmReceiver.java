package com.example.todolist;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.BoringLayout;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver
{
    private NotificationManager notificationManager;
    private String PRIMARY_CHANNEL_ID = "primary_channel_id";

    private final String ACTION_SNOOZE = "ACTION_SNOOZE";

    public final static String TASK_ID = "TASK_ID";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(intent.getAction() != null && intent.getAction().equals(ACTION_SNOOZE))
            SnoozeNotification(context, intent);
        else
            SendNotification(context, intent);
    }

    private void SendNotification(Context context, Intent taskIntent)
    {
        //Get task for contentIntent
        Container.Tasks.Initialize(context);
        int taskID = taskIntent.getIntExtra(TASK_ID, -1);
        Task task = Container.Tasks.GetTask(taskID);

        //Create content PendingIntent
        Intent mainIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(mainIntent);
        Intent intent = new Intent(context, TaskDetails.class);
        intent.putExtra(MainActivity.TASK_INTENT, task);
        taskStackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        //Create notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context, PRIMARY_CHANNEL_ID);
        notification.setContentTitle(task.title);
        notification.setContentText(context.getResources().getString(R.string.notification_content_text) + " " + AppUtils.GetDateAsString(context, task.dueDate, false));
        notification.setSmallIcon(R.drawable.ic_notification_icon);
        notification.setContentIntent(pendingIntent);
        notification.setPriority(NotificationCompat.PRIORITY_HIGH);
        notification.setAutoCancel(true);

        //Create "Snooze" action
        Intent snoozeIntent = new Intent(context, AlarmReceiver.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(MainActivity.TASK_INTENT, task);
        PendingIntent snoozePendingIntent = PendingIntent.getBroadcast(context, 0, snoozeIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notification.addAction(0, context.getResources().getString(R.string.notification_action_snooze), snoozePendingIntent);

        //Notification must have a different ID, because if more than one notification is triggered at the same time with the same ID, one notification will override another
        notificationManager.notify(task.GetID(), notification.build());
    }

    private void SnoozeNotification(Context context, Intent taskIntent)
    {
        Task task = taskIntent.getParcelableExtra(MainActivity.TASK_INTENT);
        AppUtils.SetAlarm(context, task, AlarmManager.INTERVAL_FIFTEEN_MINUTES);
        NotificationManagerCompat.from(context).cancel(task.GetID());
    }
}