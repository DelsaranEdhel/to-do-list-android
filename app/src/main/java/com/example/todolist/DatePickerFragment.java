package com.example.todolist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener
{
    public static String INTENT_DAY = "DAY";
    public static String INTENT_MONTH = "MONTH";
    public static String INTENT_YEAR = "YEAR";

    public final static String DUE_DATE_TAG = "DatePickerDue";
    public final static String REMINDER_DATE_TAG = "DatePickerReminder";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_date_picker, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        Bundle arguments = getArguments();
        int year, month, day;

        if(arguments == null)
        {
            final Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }
        else
        {
            year = arguments.getInt(INTENT_YEAR);
            month = arguments.getInt(INTENT_MONTH);
            day = arguments.getInt(INTENT_DAY);
        }

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) //executes when in Calendar dialog fragment we will click OK button
    {
        String date = Integer.toString(day) + "/" + Integer.toString(month + 1) + "/" + Integer.toString(year);

        switch(getTag())
        {
            case DUE_DATE_TAG:
                Button dueDateButton = getActivity().findViewById(R.id.select_date_button);
                dueDateButton.setText(date);

                ((TaskAdd) getActivity()).dueDateTemp = AppUtils.GetCalendarFromString(getActivity(), date, AppUtils.DATE_SLASH_FORMAT);

                //date was set - show delete date button
                Button deleteDateButton = getActivity().findViewById(R.id.delete_date_button);
                deleteDateButton.setVisibility(View.VISIBLE);
                break;
            case REMINDER_DATE_TAG:
                Button reminderDateButton = getActivity().findViewById(R.id.select_reminder_date_button);
                reminderDateButton.setText(date);

                ((TaskAdd) getActivity()).reminderDateTemp = AppUtils.GetCalendarFromString(getActivity(), date, AppUtils.DATE_SLASH_FORMAT);
                break;
        }
    }
}