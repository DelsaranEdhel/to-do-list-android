package com.example.todolist;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Text;

import java.time.chrono.MinguoChronology;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener
{
    public static String INTENT_HOUR = "HOUR";
    public static String INTENT_MINUTE = "MINUTE";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_time_picker, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        Bundle arguments = getArguments();
        int hour, minute;

        /*
        Calendar calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        */

        if(arguments == null)
        {
            hour = 12;
            minute = 0;
        }
        else
        {
            hour = arguments.getInt(INTENT_HOUR);
            minute = arguments.getInt(INTENT_MINUTE);
        }

        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
    {
        //must to do this because if minute is less than 10 it displays a minute without zero (e.g. :5 instead of :05)
        String m = ((minute < 10) ? "0" : "") + minute;
        String time = hourOfDay + ":" + m;

        Button timeDisplay = getActivity().findViewById(R.id.select_reminder_time_button);
        timeDisplay.setText(time);

        ((TaskAdd) getActivity()).reminderTimeTemp = AppUtils.GetCalendarFromString(getActivity(), time, AppUtils.TIME_GLOBAL_FORMAT);
    }
}