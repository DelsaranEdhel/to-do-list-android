package com.example.todolist;

import android.app.AlarmManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;

import androidx.annotation.DrawableRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AppUtils
{
    public final static String DATE_GLOBAL_FORMAT = "EEE MMM dd HH:mm:ss z yyyy"; //global format in this app
    public final static String DATE_SLASH_FORMAT = "dd/MM/yyyy"; //format for displaying date
    public final static String TIME_GLOBAL_FORMAT = "HH:mm"; //format to convert a time in a string to calendar object
    public final static String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm"; //format for reminder (alarm)

    public static String GetDateAsString(Context context, Calendar calendar, boolean showWhenIfCan) //return date in string based on slash format
    {
        //else if(context instanceof MainActivity) //Execute if this function was executed from the MainActivity (displaying Due Date in the MainActivity)

        //If due date is not set then set "None" text
        if(calendar == null)
            return context.getResources().getString(R.string.no_date_selected_text);
        else if(showWhenIfCan) //if we want, we can check if there will be Tomorrow or Today text
        {
            Calendar today = Calendar.getInstance();

            int dayOffset = today.get(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH);
            int monthOffset = today.get(Calendar.MONTH) - calendar.get(Calendar.MONTH);
            int yearOffset = today.get(Calendar.YEAR) - calendar.get(Calendar.YEAR);

            //If correct then set "Tomorrow" or "Today" text
            //if(yearOffset == 0 && (monthOffset == 0 || monthOffset == 1))
            if(yearOffset == 0 && monthOffset == 0)
            {
                //if(monthOffset == 1) //getActualMaximum gives amount of days in this month
                //    dayOffset = (today.get(Calendar.DAY_OF_MONTH) - today.getActualMaximum(Calendar.DAY_OF_MONTH)) - calendar.get(Calendar.DAY_OF_MONTH);

                switch (dayOffset)
                {
                    case -1: //-1 is tomorrow because we are subtracting 'today - dueDate'
                        return context.getResources().getString(R.string.tomorrow_due_date);
                    case 0:
                        return context.getResources().getString(R.string.today_due_date);
                    case 1:
                        return context.getResources().getString(R.string.yesterday_due_date);
                }
            }

            //If a year is the same as current year then do not display it
            if(calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR))
                return  Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                        Integer.toString(calendar.get(Calendar.MONTH) + 1);
        }

        //Return default due date string format
        return  Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                Integer.toString(calendar.get(Calendar.MONTH) + 1) + "/" +
                Integer.toString(calendar.get(Calendar.YEAR));
    }

    public static String GetTimeAsString(Context context, Calendar calendar)
    {
        if(calendar == null)
            return context.getResources().getString(R.string.no_date_selected_text);

        return calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                ((calendar.get(Calendar.MINUTE) < 10) ? "0" : "") + calendar.get(Calendar.MINUTE);
    }

    public static Calendar GetCalendarFromString(Context context, String textValue, final String format)
    {
        Calendar calendar = Calendar.getInstance();

        //Always "None", because always this text is used when saving to the file
        //When there is no dueDate or reminderData, it is saved by value "None" in the file
        //No matter what language it is
        if(textValue.equals("None"))
            calendar = null;
        else
        {
            try
            {
                //Locale is always "en", because we are displaying the data always like "13/2/2022"
                //So we are never using text, as "Monday", "Poniedziałek", "January" and so on
                SimpleDateFormat dateFormat = new SimpleDateFormat(format, new Locale("en"));
                Date date = dateFormat.parse(textValue);
                calendar.setTime(date);
            }
            catch (ParseException parseException)
            {
                Log.e("AppUtils", "ParseException - GetCalendarFromString()");
                parseException.getStackTrace();
            }
        }

        return calendar;
    }

    public static int GetTitleColor(Context context, Task.Priority priority) //set task title color based on a priority value
    {
        if(priority == Task.Priority.LOW)
            return context.getResources().getColor(R.color.priorityLowColor);
        else if(priority == Task.Priority.MEDIUM)
            return context.getResources().getColor(R.color.priorityMediumColor);
        else
            return context.getResources().getColor(R.color.priorityHighColor);
    }

    public static String GetPriorityName(Context context, String prioritySpinnerValue, boolean defaultLanguage)
    {
        String priorityValue;

        //Priority text - first letter is upper case, the rest of text is lower case ("Low", "Medium", "High")
        prioritySpinnerValue = prioritySpinnerValue.toLowerCase();
        prioritySpinnerValue = Character.toUpperCase(prioritySpinnerValue.charAt(0)) + prioritySpinnerValue.substring(1);

        //When defaultLanguage is true, we are getting values for code, for example when saving priority data 'low' we are always using "LOW"
        if(defaultLanguage)
        {
            if(prioritySpinnerValue.equals(context.getResources().getString(R.string.priority_low)))
                priorityValue = "LOW";
            else if(prioritySpinnerValue.equals(context.getResources().getString(R.string.priority_medium)))
                priorityValue = "MEDIUM";
            else
                priorityValue = "HIGH";
        }
        else //When defaultLanguage is false, we are getting text in specific language to display on the screen
        {
            //We are creating locale "en", which is default locale to compare with given value
            Configuration configuration = new Configuration(context.getResources().getConfiguration());
            configuration.setLocale(new Locale("en"));

            if(prioritySpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.priority_low)))
                priorityValue = context.getResources().getString(R.string.priority_low);
            else if(prioritySpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.priority_medium)))
                priorityValue = context.getResources().getString(R.string.priority_medium);
            else
                priorityValue = context.getResources().getString(R.string.priority_high);
        }

        return priorityValue;
    }

    public static String GetRepeatName(Context context, String repeatSpinnerValue, boolean defaultLanguage)
    {
        String repeatValue;

        //Repeat text - first letter is upper case, the rest of text is lower case ("None", "Daily", "Weekly")
        repeatSpinnerValue = repeatSpinnerValue.toLowerCase();
        repeatSpinnerValue = Character.toUpperCase(repeatSpinnerValue.charAt(0)) + repeatSpinnerValue.substring(1);

        //When defaultLanguage is true, we are getting values for code, for example when saving repeat data 'daily' we are always using "DAILY"
        if(defaultLanguage)
        {
            if(repeatSpinnerValue.equals(context.getResources().getString(R.string.repeat_none)))
                repeatValue = "NONE";
            else if(repeatSpinnerValue.equals(context.getResources().getString(R.string.repeat_daily)))
                repeatValue = "DAILY";
            else if(repeatSpinnerValue.equals(context.getResources().getString(R.string.repeat_weekly)))
                repeatValue = "WEEKLY";
            else if(repeatSpinnerValue.equals(context.getResources().getString(R.string.repeat_monthly)))
                repeatValue = "MONTHLY";
            else
                repeatValue = "YEARLY";
        }
        else //When defaultLanguage is false, we are getting text in specific language to display on the screen
        {
            //We are creating locale "en", which is default locale to compare with given value
            Configuration configuration = new Configuration(context.getResources().getConfiguration());
            configuration.setLocale(new Locale("en"));

            if(repeatSpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.repeat_none)))
                repeatValue = context.getResources().getString(R.string.repeat_none);
            else if(repeatSpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.repeat_daily)))
                repeatValue = context.getResources().getString(R.string.repeat_daily);
            else if(repeatSpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.repeat_weekly)))
                repeatValue = context.getResources().getString(R.string.repeat_weekly);
            else if(repeatSpinnerValue.equals(context.createConfigurationContext(configuration).getResources().getString(R.string.repeat_monthly)))
                repeatValue = context.getResources().getString(R.string.repeat_monthly);
            else
                repeatValue = context.getResources().getString(R.string.repeat_yearly);
        }

        return repeatValue;
    }

    public static void SetAlarm(Context context, Task task)
    {
        long triggerTime = System.currentTimeMillis() + (task.reminderTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis());
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, triggerTime, task.GetPendingIntent(context));
    }

    public static void SetAlarm(Context context, Task task, long snooze)
    {
        long triggerTime = System.currentTimeMillis() + (task.reminderTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()) + snooze;
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, triggerTime, task.GetPendingIntent(context));
    }
}
