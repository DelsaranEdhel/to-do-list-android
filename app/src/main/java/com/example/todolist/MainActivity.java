package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.BoringLayout;
import android.transition.Explode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity
{
    public final static String TASK_INTENT = "com.example.android.todolist.extra.TASK_INTENT"; //bundle where task is being saved

    private RecyclerView recyclerView;
    private TaskAdapter taskAdapter;

    public enum SortType //save, what sort type is set
    {
        alphabet(0),
        priority(1),
        dueDate(2),
        createTime(3);

        private int number;

        private SortType(int number)
        {
            this.number = number;
        }

        public int getNumber()
        {
            return number;
        }
    }
    private SortType tasksSortType = SortType.alphabet; //default value (will be replaced, when launching an app)

    private NotificationManager notificationManager;
    private String PRIMARY_CHANNEL_ID = "primary_channel_id";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
            //getWindow().setSharedElementEnterTransition(new Explode());
            //getWindow().setSharedElementExitTransition(new Explode());
            getWindow().setExitTransition(new Explode());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Always no Night Mode
        //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        Container.Tasks.Initialize(this);

        //Set toolbar
        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        //Reading from shared preferences
        SharedPreferences sharedPreferences = this.getSharedPreferences(getString(R.string.preference_sort_type), Context.MODE_PRIVATE);
        tasksSortType = SortType.valueOf(sharedPreferences.getString(getString(R.string.preference_sort_type), SortType.alphabet.name()));

        //Create RecyclerView
        recyclerView = findViewById(R.id.recyclerView);
        taskAdapter = new TaskAdapter(this, Container.Tasks.GetList(), this);
        recyclerView.setAdapter(taskAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        registerForContextMenu(recyclerView);


        Context context = this;
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive)
            {
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE)
                {
                    int textColor = getResources().getColor(R.color.white);
                    int completeBackgroundColor = getResources().getColor(R.color.swipeCompleteColor);
                    int deleteBackgroundColor = getResources().getColor(R.color.swipeDeleteColor);

                    //ItemView has 10 dp margin, so convert dp to pixels and add it to the swipe background
                    float scale = getResources().getDisplayMetrics().density;
                    int dpAsPixels = (int) (10*scale + 0.5f);

                    if(dX > 0)
                    {
                        View itemView = viewHolder.itemView;

                        //Background
                        ColorDrawable background = new ColorDrawable(completeBackgroundColor);
                        //background.setBounds(0, itemView.getTop(), itemView.getLeft() + (int)dX, itemView.getBottom()); //also works
                        background.setBounds(itemView.getLeft(), itemView.getTop() + dpAsPixels, (int)dX, itemView.getBottom() - dpAsPixels);
                        background.draw(c);

                        //Icon
                        Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_done);
                        icon.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                        //icon.setBounds(0, itemView.getTop(), icon.getIntrinsicWidth(), icon.getIntrinsicHeight()); //display icon in the left row
                        int widthMargin = 50;
                        int heightMargin = itemView.getHeight() / 2 - icon.getIntrinsicHeight() / 2;
                        icon.setBounds(0 + widthMargin, itemView.getTop() + heightMargin, icon.getIntrinsicWidth() + widthMargin, icon.getIntrinsicHeight() + heightMargin + itemView.getTop());
                        icon.draw(c);

                        //Text
                        Paint paint = new Paint();
                        paint.setColor(textColor);
                        paint.setTextSize(40f);
                        paint.setTextAlign(Paint.Align.LEFT);
                        int textPosX = widthMargin + icon.getIntrinsicWidth() + 25;
                        int textPosY = (itemView.getHeight() / 2) + (icon.getIntrinsicHeight() / 4) + itemView.getTop();
                        c.drawText(getResources().getString(R.string.swipe_done), textPosX, textPosY, paint);

                        //Fading out task while swiping
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int width = displayMetrics.widthPixels;
                        float alphaValue = dX / width;
                        itemView.setAlpha(1 - alphaValue);
                    }
                    else
                    {
                        View itemView = viewHolder.itemView;

                        //Background
                        ColorDrawable background = new ColorDrawable(deleteBackgroundColor);
                        background.setBounds(itemView.getRight() + (int)dX, itemView.getTop() + dpAsPixels, itemView.getRight(), itemView.getBottom() - dpAsPixels);
                        background.draw(c);

                        //Icon
                        Drawable icon = ContextCompat.getDrawable(context, R.drawable.ic_delete_icon);
                        icon.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
                        //icon.setBounds(0, itemView.getTop(), icon.getIntrinsicWidth(), icon.getIntrinsicHeight()); //display icon in the left row
                        int widthMargin = 50;
                        int heightMargin = itemView.getHeight() / 2 - icon.getIntrinsicHeight() / 2;
                        icon.setBounds(itemView.getRight() - icon.getIntrinsicWidth() - widthMargin, itemView.getTop() + heightMargin, itemView.getRight() - widthMargin, icon.getIntrinsicHeight() + heightMargin + itemView.getTop());
                        icon.draw(c);

                        //Text
                        Paint paint = new Paint();
                        paint.setColor(textColor);
                        paint.setTextSize(40f);
                        paint.setTextAlign(Paint.Align.RIGHT);
                        int textPosX = itemView.getRight() - icon.getIntrinsicWidth() - widthMargin - 25;
                        int textPosY = (itemView.getHeight() / 2) + (icon.getIntrinsicHeight() / 4) + itemView.getTop();
                        c.drawText(getResources().getString(R.string.swipe_delete), textPosX, textPosY, paint);

                        //Fading out task while swiping
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        int width = displayMetrics.widthPixels;
                        float alphaValue = -dX / width;
                        itemView.setAlpha(1 - alphaValue);
                    }
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target)
            {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction)
            {
                int taskID = taskAdapter.getItem(viewHolder.getAdapterPosition()).GetID();

                //Swipe left is delete the task
                if(direction == ItemTouchHelper.LEFT)
                {
                    //Get task position in recyclerView
                    final int position = viewHolder.getAdapterPosition();
                    //Get Task object, which we are deleting
                    final Task deletedTask = taskAdapter.getItem(position);

                    //taskAdapter.removeItem(position);
                    //((AlarmManager)getSystemService(ALARM_SERVICE)).cancel(deletedTask.GetPendingIntent(context));

                    //Delete the task and update notify recyclerView
                    Container.Tasks.DeleteTask(taskID);
                    onStart();

                    //Create snackbar for undo delete task action
                    Snackbar snackbar = Snackbar.make(recyclerView, deletedTask.title, Snackbar.LENGTH_LONG);
                    snackbar.setAction(getResources().getString(R.string.undo_action), new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            //Restore task - add it to the list and set reminder alarm if exists
                            taskAdapter.restoreItem(deletedTask, position);
                            //Scroll to restored task
                            recyclerView.scrollToPosition(position);
                            findViewById(R.id.no_tasks_message).setVisibility(View.INVISIBLE);
                        }
                    });
                    snackbar.show();
                }
                else if(direction == ItemTouchHelper.RIGHT) //swipe right is complete the task
                {
                    Container.Tasks.CompleteTask(taskID);
                    onStart();
                }
            }
        });

        itemTouchHelper.attachToRecyclerView(recyclerView);

        CreateNotificationChannel();
    }

    @Override
    protected void onStart()
    {
        if(Container.Tasks.GetList().isEmpty()) //display "No tasks" message when there is no tasks created
            findViewById(R.id.no_tasks_message).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.no_tasks_message).setVisibility(View.INVISIBLE);

        taskAdapter = new TaskAdapter(this, Container.Tasks.GetList(), this);
        recyclerView.setAdapter(taskAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        UpdateTasksView();

        super.onStart();
    }

    public void UpdateTasksView()
    {
        SortBy(tasksSortType);
        taskAdapter.notifyDataSetChanged();
    }

    private void CreateNotificationChannel()
    {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID, getResources().getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription(getResources().getString(R.string.notification_channel_description));
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) //for floating menu - receive info from recycler view
    {
        int taskID = taskAdapter.getItem(taskAdapter.getPosition()).GetID();

        switch (item.getItemId())
        {
            case R.id.delete_floating_menu:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.delete_dialog_alert);
                builder.setPositiveButton(R.string.delete_dialog_positive_button, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Container.Tasks.DeleteTask(taskID);
                        //UpdateTasksView();
                        onStart();
                    }
                });
                builder.setNegativeButton(R.string.delete_dialog_negative_button, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                    }
                });

                builder.show();

                break;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.main_sort:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.sort_option);
                builder.setSingleChoiceItems(R.array.sort_options, tasksSortType.getNumber(), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch(which)
                        {
                            case 0: //Alphabetically
                                SortBy(SortType.alphabet);
                                Container.Tasks.SetSortType(SortType.alphabet);
                                break;
                            case 1: //Priority
                                SortBy(SortType.priority);
                                Container.Tasks.SetSortType(SortType.priority);
                                break;
                            case 2: //Due Date
                                SortBy(SortType.dueDate);
                                Container.Tasks.SetSortType(SortType.dueDate);
                                break;
                            case 3: //Create Time
                                SortBy(SortType.createTime);
                                Container.Tasks.SetSortType(SortType.createTime);
                                break;
                        }

                        dialog.cancel();
                        onStart();
                    }
                });
                builder.setNegativeButton(R.string.cancel_button_text, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                    }
                });
                builder.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void SortBy(SortType sortBy)
    {
        tasksSortType = sortBy;

        switch(sortBy)
        {
            case alphabet:
                //Collections.sort(tasksList, ((o1, o2) -> o1.title.compareTo(o2.title)));
                Collections.sort(Container.Tasks.GetList(), new Comparator<Task>()
                {
                    @Override
                    public int compare(Task o1, Task o2)
                    {
                        if(o1.isCompleted)
                            return (o2.isCompleted) ? 0 : 1;

                        if(o2.isCompleted)
                            return -1;

                        return o1.title.compareTo(o2.title);
                    }
                });
                break;
            case priority:
                //Collections.sort(tasksList, ((o1, o2) -> Integer.compare(o2.priority.getNumber(), o1.priority.getNumber())));
                Collections.sort(Container.Tasks.GetList(), new Comparator<Task>()
                {
                    @Override
                    public int compare(Task o1, Task o2)
                    {
                        if(o1.isCompleted)
                            return (o2.isCompleted) ? 0 : 1;

                        if(o2.isCompleted)
                            return -1;

                        return Integer.compare(o2.priority.getNumber(), o1.priority.getNumber());
                    }
                });
                break;
            case dueDate:
                Collections.sort(Container.Tasks.GetList(), new Comparator<Task>()
                {
                    @Override
                    public int compare(Task o1, Task o2)
                    {
                        /*
                         * return 0 - do nothing
                         * return -1 - move element o2 after element o1
                         * return 1 - move element o2 before element o1
                         */

                        if(o1.isCompleted)
                            return (o2.isCompleted) ? 0 : 1;

                        if(o2.isCompleted)
                            return -1;

                        if(o1.dueDate == null)
                            return (o2.dueDate == null) ? 0 : 1;

                        if(o2.dueDate == null)
                            return -1;

                        return o1.dueDate.compareTo(o2.dueDate);
                    }
                });
                break;
            case createTime:
                //Collections.sort(tasksList, ((o1, o2) -> o2.createTime.compareTo(o1.createTime)));
                Collections.sort(Container.Tasks.GetList(), new Comparator<Task>()
                {
                    @Override
                    public int compare(Task o1, Task o2)
                    {
                        if(o1.isCompleted)
                            return (o2.isCompleted) ? 0 : 1;

                        if(o2.isCompleted)
                            return -1;

                        return o2.createTime.compareTo(o1.createTime);
                    }
                });
                break;
        }
    }

    public void AddTask(View view) //OnClick method for FAB - go to TaskAdd activity
    {
        Intent intent = new Intent(this, TaskAdd.class);
        intent.putExtra(TaskAdd.INTENT_ID, TaskAdd.INTENT_FROM_MAIN_ACTIVITY); //inform, that we will be creating a new task
        startActivity(intent);
    }

    //public void DisplayTask(Task task, Pair<View, String> titleView, Pair<View, String> dueDateView, Pair<View, String> descriptionView)
    public void DisplayTask(Task task)
    {
        Intent intent = new Intent(this, TaskDetails.class);
        intent.putExtra(TASK_INTENT, task);

        //These animations are handled only in lollipop or higher
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            //Use for Shared Elements
            //ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, titleView, dueDateView, descriptionView);
            //intent.putExtra("titleTransition", titleView.second);

            ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this);
            startActivity(intent, activityOptionsCompat.toBundle());
        }
        else
            startActivity(intent);
    }

    @Override
    protected void onStop()
    {
        Container.Tasks.SaveData(this); //save tasks/data when closing app or moving to another activity
        super.onStop();
    }
}