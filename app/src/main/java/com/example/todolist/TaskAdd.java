package com.example.todolist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.material.switchmaterial.SwitchMaterial;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TaskAdd extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    public final static String INTENT_ID = "INTENT_ID"; //determines from which activity, this activity was launched
    public final static String INTENT_FROM_MAIN_ACTIVITY = "FromMainActivity"; //if was launched from MainActivity then we will be creating task
    public final static String INTENT_FROM_TASK_DETAILS = "FromTaskDetails"; //if was launched from TaskDetails then we will be editing task

    private AutoCompleteTextView prioritySpinner;
    private AutoCompleteTextView repeatSpinner;
    private EditText titleEditText;
    private EditText descriptionEditText;
    private Button dueDateButton;
    private Button dueDateDeleteButton;
    private SwitchMaterial reminderToggle;
    private ConstraintLayout reminderViewGroup;
    private Button reminderDateButton;
    private Button reminderTimeButton;

    private Intent intent;
    private AlarmManager alarmManager;

    public Calendar dueDateTemp = null;
    public Calendar reminderDateTemp = null;
    public Calendar reminderTimeTemp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_add);

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.task_add_toolbar_title));
        setSupportActionBar(toolbar);

        //Creating Priority Spinner
        prioritySpinner = findViewById(R.id.priority_spinner);
        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this, R.array.priority_spinner, R.layout.spinner_item_layout); //default layout -> android.R.layout.simple_spinner_item
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //prioritySpinner.setSelection(0) won't work because we are using a different type of Spinner - AutoCompleteTextView (based on text)
        //also we need to set default value before setAdapter
        prioritySpinner.setText(priorityAdapter.getItem(0)); //set default value ("Low")
        prioritySpinner.setOnItemSelectedListener(this);

        //Creating Repeat Spinner
        repeatSpinner = findViewById(R.id.repeat_spinner);
        ArrayAdapter<CharSequence> repeatAdapter = ArrayAdapter.createFromResource(this, R.array.repeat_spinner, R.layout.spinner_item_layout); //default layout -> android.R.layout.simple_spinner_item
        repeatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        repeatSpinner.setText(repeatAdapter.getItem(0)); //set default value ("None")
        repeatSpinner.setOnItemSelectedListener(this);

        titleEditText = findViewById(R.id.title_editText_add);
        descriptionEditText = findViewById(R.id.description_editText_add);

        dueDateButton = findViewById(R.id.select_date_button);
        dueDateDeleteButton = findViewById(R.id.delete_date_button);

        reminderToggle = findViewById(R.id.reminder_toggle);
        reminderDateButton = findViewById(R.id.select_reminder_date_button);
        reminderTimeButton = findViewById(R.id.select_reminder_time_button);

        //Get reminder view elements and turn them off
        reminderViewGroup = findViewById(R.id.reminder_view_group);
        reminderViewGroup.setVisibility(View.GONE);

        //Create default reminder toggle
        reminderToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    reminderDateButton.setText(AppUtils.GetDateAsString(TaskAdd.this, Calendar.getInstance(), false));
                    reminderTimeButton.setText(getResources().getString(R.string.default_reminder_time));
                    reminderViewGroup.setVisibility(View.VISIBLE);
                }
                else
                    reminderViewGroup.setVisibility(View.GONE);
            }
        });

        intent = getIntent();
        /*
        In java we must compare Strings using 'equal' method, because Strings are objects
        and when using '==' we compare references to these objects
        */
        if(intent.getStringExtra(INTENT_ID).equals(INTENT_FROM_TASK_DETAILS)) //if tasks comes from TaskDetails then we will be editing the task
        {
            toolbar.setTitle(getResources().getString(R.string.task_edit_toolbar_title));

            Task task = intent.getParcelableExtra(TaskDetails.TASK_DETAILS_INTENT);

            titleEditText.setText(task.title);
            descriptionEditText.setText(task.description);
            //prioritySpinner.setSelection(task.priority.getNumber()) won't work because we are using a different type of Spinner - AutoCompleteTextView (based on text)
            prioritySpinner.setText(priorityAdapter.getItem(task.priority.getNumber()));
            repeatSpinner.setText(repeatAdapter.getItem(task.repeat.getNumber()));
            dueDateButton.setText(AppUtils.GetDateAsString(this, task.dueDate, false)); //if dueDate is null, it will return "None"
            if(task.dueDate == null)
                dueDateDeleteButton.setVisibility(View.GONE);
            else
                dueDateDeleteButton.setVisibility(View.VISIBLE);

            if(task.reminderTime != null)
            {
                String time = AppUtils.GetTimeAsString(this, task.reminderTime);
                String date = AppUtils.GetDateAsString(this, task.reminderTime, false);
                reminderDateButton.setText(date);
                reminderTimeButton.setText(time);

                reminderToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        if(isChecked)
                            reminderViewGroup.setVisibility(View.VISIBLE);
                        else
                        {
                            reminderViewGroup.setVisibility(View.GONE);
                            alarmManager.cancel(task.GetPendingIntent(TaskAdd.this));
                            task.reminderTime = null;
                            reminderDateButton.setText(AppUtils.GetDateAsString(TaskAdd.this, Calendar.getInstance(), false));
                            reminderTimeButton.setText(getResources().getString(R.string.default_reminder_time));
                        }
                    }
                });

                reminderToggle.setChecked(true);
            }

            if(task.dueDate != null)
                dueDateTemp = (Calendar) task.dueDate.clone();
            if(task.reminderTime != null)
            {
                reminderDateTemp = (Calendar) task.reminderTime.clone();
                reminderTimeTemp = (Calendar) task.reminderTime.clone();
            }
        }

        //Strange, but works, every time we are setting through code a default value after setAdapter it show only one item
        //So we are setting adapter at the end
        prioritySpinner.setAdapter(priorityAdapter);
        repeatSpinner.setAdapter(repeatAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) //what to do when selecting item on spinner
    {
        if(parent.getId() == repeatSpinner.getId()
                && Task.Repeat.valueOf(AppUtils.GetRepeatName(this, repeatSpinner.getText().toString(), true)) != Task.Repeat.NONE
                && dueDateButton.getText().equals(getResources().getString((R.string.no_date_selected_text))))
        {
            Calendar calendar = Calendar.getInstance();
            dueDateButton.setText(AppUtils.GetDateAsString(this, calendar, false));
            dueDateTemp = calendar;
            dueDateDeleteButton.setVisibility(View.VISIBLE);
        }

        /*
        //if repeat spinner value was set to something different than "None" and due date is not set, then we must set a due date
        if(parent.getId() == repeatSpinner.getId() //check if it is repeat spinner
                && Task.Repeat.valueOf(repeatSpinner.getText().toString().toUpperCase()) != Task.Repeat.NONE //check if repeat spinner value is different than "None"
                && dueDateButton.getText().equals(getResources().getString(R.string.no_date_selected_text))) //check if due date is not already set
        {
            Calendar calendar = Calendar.getInstance();
            dueDateButton.setText(AppUtils.GetDateAsString(this, calendar, false)); //set default (today's) date
            dueDateTemp = calendar;
            dueDateDeleteButton.setVisibility(View.VISIBLE); //if due date was set, then show Delete due date button
        }
         */
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) //what to do when nothing is selected on spinner
    {

    }

    public void ShowDatePickerDialog(View view) //OnClick function for Due and Reminder dates
    {
        DialogFragment dialogFragment = new DatePickerFragment();

        switch(view.getId())
        {
            case R.id.select_date_button:
                if(dueDateTemp != null)
                {
                    Bundle arguments = new Bundle();
                    arguments.putInt(DatePickerFragment.INTENT_DAY, dueDateTemp.get(Calendar.DAY_OF_MONTH));
                    arguments.putInt(DatePickerFragment.INTENT_MONTH, dueDateTemp.get(Calendar.MONTH));
                    arguments.putInt(DatePickerFragment.INTENT_YEAR, dueDateTemp.get(Calendar.YEAR));
                    dialogFragment.setArguments(arguments);
                }
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.DUE_DATE_TAG);
                break;
            case R.id.select_reminder_date_button:
                if(reminderDateTemp != null)
                {
                    Bundle arguments = new Bundle();
                    arguments.putInt(DatePickerFragment.INTENT_DAY, reminderDateTemp.get(Calendar.DAY_OF_MONTH));
                    arguments.putInt(DatePickerFragment.INTENT_MONTH, reminderDateTemp.get(Calendar.MONTH));
                    arguments.putInt(DatePickerFragment.INTENT_YEAR, reminderDateTemp.get(Calendar.YEAR));
                    dialogFragment.setArguments(arguments);
                }
                dialogFragment.show(getSupportFragmentManager(), DatePickerFragment.REMINDER_DATE_TAG);
                break;
        }
    }

    public void ShowTimePickerDialogReminder(View view) //OnClick function for Reminder Time
    {
        DialogFragment dialogFragment = new TimePickerFragment();

        if(reminderTimeTemp != null)
        {
            Bundle arguments = new Bundle();
            arguments.putInt(TimePickerFragment.INTENT_HOUR, reminderTimeTemp.get(Calendar.HOUR_OF_DAY));
            arguments.putInt(TimePickerFragment.INTENT_MINUTE, reminderTimeTemp.get(Calendar.MINUTE));
            dialogFragment.setArguments(arguments);
        }

        dialogFragment.show(getSupportFragmentManager(), "TimeReminder");
    }

    public void DeleteDueDate(View view) //OnClick for deleting due date, if was set
    {
        dueDateButton.setText(getResources().getString(R.string.no_date_selected_text));
        dueDateTemp = null;
        dueDateDeleteButton.setVisibility(View.GONE);

        if(Task.Repeat.valueOf(AppUtils.GetRepeatName(this, repeatSpinner.getText().toString(), true)) != Task.Repeat.NONE)
        {
            //repeatSpinner.setText(AppUtils.GetRepeatName(this, "None", false));

            ArrayAdapter<CharSequence> repeatAdapter = ArrayAdapter.createFromResource(this, R.array.repeat_spinner, R.layout.spinner_item_layout); //default layout -> android.R.layout.simple_spinner_item
            repeatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            repeatSpinner.setText(repeatAdapter.getItem(0)); //set default value ("None")
            //repeatSpinner.setOnItemSelectedListener(this);
            repeatSpinner.setAdapter(repeatAdapter);
        }


        //if deleting due date, we must remember to set the repeat spinner to "None"
        //if(Task.Repeat.valueOf(repeatSpinner.getText().toString().toUpperCase()) != Task.Repeat.NONE)
        //    repeatSpinner.setSelection(0);
    }

    public void Add(View view)
    {
        String title = titleEditText.getText().toString();
        if(title.trim().isEmpty() || title.equals("")) //if title was not set, then set default title name
            title = getResources().getString(R.string.default_task_name);

        String description = descriptionEditText.getText().toString();

        Task.Priority priority = Task.Priority.valueOf(AppUtils.GetPriorityName(this, prioritySpinner.getText().toString(), true));
        Task.Repeat repeat = Task.Repeat.valueOf(AppUtils.GetRepeatName(this, repeatSpinner.getText().toString(), true));

        Calendar createdTime;
        if(intent.getStringExtra(INTENT_ID).equals(INTENT_FROM_TASK_DETAILS))
            createdTime = ((Task) intent.getParcelableExtra(TaskDetails.TASK_DETAILS_INTENT)).createTime;
        else
            createdTime = Calendar.getInstance();

        Calendar dueDate;
        if(dueDateTemp != null)
            dueDate = (Calendar) dueDateTemp.clone();
        else
            dueDate = null;

        Calendar reminder;
        if(reminderToggle.isChecked())
            reminder = AppUtils.GetCalendarFromString(this, reminderDateButton.getText().toString() + " " + reminderTimeButton.getText().toString(), AppUtils.DATE_TIME_FORMAT);
        else
            reminder = null;

        Task newTask = null;
        if(intent.getStringExtra(INTENT_ID).equals(INTENT_FROM_TASK_DETAILS)) //when editing the task, it's id remain the same
        {
            newTask = new Task(title, description, priority, repeat, createdTime, dueDate, reminder, false, ((Task)intent.getParcelableExtra(TaskDetails.TASK_DETAILS_INTENT)).GetID());
            Container.Tasks.EditTask(newTask);
        }
        else if(intent.getStringExtra(INTENT_ID).equals(INTENT_FROM_MAIN_ACTIVITY))
        {
            newTask = new Task(title, description, priority, repeat, createdTime, dueDate, reminder, false);
            Container.Tasks.AddTask(newTask);
        }
        else
            Log.e("TaskAdd", "Task intent is not from TaskDetails or MainActivity!");

        if(reminderToggle.isChecked())
            AppUtils.SetAlarm(this, newTask);

        Container.Tasks.SaveData(this);
        finish();
    }

    public void Cancel(View view) //OnClick function - cancel adding or editing task
    {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}