package com.example.todolist;

import android.app.AlarmManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;

public class Container
{
    public static class Tasks extends Application
    {
        private static LinkedList<Task> tasksList = new LinkedList<Task>();

        private static final String savedTasksFile = "saved_tasks.txt"; //file where all tasks are being saved
        private static final String propertySeparator = ";prse;"; //for separating properties of one task e.g. "Title;prse;Description;prse;" and so on
        private static final String lineSeparator = ";lise;"; //for separating lines with tasks, we can not use '\n', because in task description may be 'n'

        private static MainActivity.SortType tasksSortType = MainActivity.SortType.alphabet; //default value (will be replaced, when launching an app)

        private static AlarmManager alarmManager;
        private static Context globalContext;

        private static boolean listModifiedFlag = false; //informs that list was modified and before we load old data from file, we must first save new data to file

        public static void Initialize(Context context)
        {
            if(listModifiedFlag)
            {
                SaveData(context);
                listModifiedFlag = false;
            }
            LoadData(context);
            alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            globalContext = context;
        }

        public static void SetSortType(MainActivity.SortType sortType)
        {
            tasksSortType = sortType;
        }

        public static LinkedList<Task> GetList()
        {
            return tasksList;
        }

        public static void SaveData(Context context) //store data/all tasks to the file
        {
            //1. Creating string, where each task is a separate line and each line is a single task values separated with comma
            String content = "";
            for(int i = 0; i < tasksList.size(); i++)
            {
                Task task = tasksList.get(i);
                String line = task.title + propertySeparator + task.description + propertySeparator + task.priority.name() + propertySeparator +
                        task.repeat.name() + propertySeparator + task.createTime.getTime().toString() + propertySeparator +
                        (task.dueDate == null ? "None" : task.dueDate.getTime().toString()) + propertySeparator +
                        (task.reminderTime == null ? "None" : task.reminderTime.getTime().toString()) + propertySeparator +
                        task.isCompleted + propertySeparator + task.GetID();

                content += line + lineSeparator;
            }

            //2. Getting file and storing string in it
            File path = context.getFilesDir();
            File tasksFile = new File(path, savedTasksFile);
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(tasksFile);
                fileOutputStream.write(content.getBytes());
                fileOutputStream.close();
            }
            catch (FileNotFoundException fileException)
            {
                Log.e("TasksList", "File not found when saving file!");
            }
            catch (IOException IOexception)
            {
                Log.e("TasksList", "IOException when saving file!");
            }

            //saving to shared preferences
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_sort_type), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(context.getString(R.string.preference_sort_type), tasksSortType.name());
            editor.apply();
        }

        public static void LoadData(Context context)
        {
            //1. Get file to read from
            File path = context.getFilesDir();
            File tasksFile = new File(path, savedTasksFile);

            LinkedList<Task> newTasksList = new LinkedList<Task>();

            //2. Check if the file is empty
            if(tasksFile.length() == 0)
                return; //return newTasksList;

            //3. Convert string to task objects and create list from them
            int length = (int) tasksFile.length();
            byte[] bytes = new byte[length];
            String content;
            try
            {
                FileInputStream fileInputStream = new FileInputStream(tasksFile);
                fileInputStream.read(bytes);
                content = new String(bytes);

                String[] separateTasks = content.split(lineSeparator);

                for(int i = 0; i < separateTasks.length; i++)
                {
                    String[] line = separateTasks[i].split(propertySeparator);
                    Task task = new Task(line[0], line[1],
                            Task.Priority.valueOf(line[2]),
                            Task.Repeat.valueOf(line[3]),
                            AppUtils.GetCalendarFromString(context, line[4], AppUtils.DATE_GLOBAL_FORMAT),
                            AppUtils.GetCalendarFromString(context, line[5], AppUtils.DATE_GLOBAL_FORMAT),
                            AppUtils.GetCalendarFromString(context, line[6], AppUtils.DATE_GLOBAL_FORMAT),
                            line[7].equals("true"),
                            Integer.parseInt(line[8]));

                    newTasksList.add(task);
                }
            }
            catch (FileNotFoundException fileException)
            {
                Log.e("TasksList", "File not found when reading file!");
            }
            catch (IOException IOexception)
            {
                Log.e("TasksList", "IOException when reading file!");
            }

            tasksList = newTasksList;
        }

        public static Task GetTask(int id)
        {
            for(Task task : tasksList)
            {
                if(task.GetID() == id)
                    return task;
            }

            throw new RuntimeException("Task with id " + id + " was not found!");
        }

        public static void AddTask(Task newTask)
        {
            tasksList.add(newTask);
            listModifiedFlag = true;
        }

        public static void DeleteTask(int taskID)
        {
            Task task = GetTask(taskID);

            if(task.reminderTime != null)
                alarmManager.cancel(task.GetPendingIntent(globalContext));

            tasksList.remove(task);

            listModifiedFlag = true;
        }

        public static void EditTask(Task editedTask)
        {
            for(int i = 0; i < tasksList.size(); i++)
            {
                if(tasksList.get(i).GetID() == editedTask.GetID())
                    tasksList.set(i, editedTask);
            }

            listModifiedFlag = true;
        }

        public static void CompleteTask(int taskID) //boolean state)
        {
            Task task = GetTask(taskID);
            boolean state = !task.isCompleted;

            if(state)
            {
                if((task.repeat != Task.Repeat.NONE) && (task.dueDate != null)) //check if task has repeat value and if does, clone this task
                {
                    Task newTask = new Task(task); // this will work, because it will assign a reference to this object. It means we work on the same object, not copy!
                    task.repeat = Task.Repeat.NONE; //set repeat value to NONE for completed task

                    switch(newTask.repeat) //set new date based on repeat value
                    {
                        case DAILY:
                            newTask.dueDate.add(Calendar.DAY_OF_MONTH, 1);
                            if(newTask.reminderTime != null)
                                newTask.reminderTime.add(Calendar.DAY_OF_MONTH, 1);
                            break;
                        case WEEKLY:
                            newTask.dueDate.add(Calendar.WEEK_OF_MONTH, 1);
                            if(newTask.reminderTime != null)
                                newTask.reminderTime.add(Calendar.WEEK_OF_MONTH, 1);
                            break;
                        case MONTHLY:
                            newTask.dueDate.add(Calendar.MONTH, 1);
                            if(newTask.reminderTime != null)
                                newTask.reminderTime.add(Calendar.MONTH, 1);
                            break;
                        case YEARLY:
                            newTask.dueDate.add(Calendar.YEAR, 1);
                            if(newTask.reminderTime != null)
                                newTask.reminderTime.add(Calendar.YEAR, 1);
                            break;
                    }

                    if(newTask.reminderTime != null)
                        AppUtils.SetAlarm(globalContext, newTask);

                    tasksList.add(newTask);
                }

                if(task.reminderTime != null)
                {
                    alarmManager.cancel(task.GetPendingIntent(globalContext));
                    task.reminderTime = null;
                }
            }

            task.isCompleted = state;
            listModifiedFlag = true;
        }
    }
}
